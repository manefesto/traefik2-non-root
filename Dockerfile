FROM traefik:v2.4
RUN apk add --update --no-cache libcap &&\
    addgroup -S traefik && \
    adduser -S -g traefik traefik &&\
    setcap 'cap_net_bind_service=+ep' $(which traefik)
USER traefik